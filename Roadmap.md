# Roadmap

- [ ] Archive to GitLab
- [ ] Archive to https://web.archive.org/

- [ ] Make NDEList Astro component
- [ ] Add a secure form for sharing NDE (with required and optional fields)
- [ ] Add a secure contact form
- [ ] Add Newsletter

## History

2022:

- [x] Transfer all NDEs
- [x] Transfer all pages
- [x] up to date all page content
- [x] Add additional links to all NDES
- [x] Improve main page - content and layout
- [x] Publish to Github
- [x] Add back button at the bottom of an NDE page 
- [ ] Make SEO friendly!
  - [x] Lazy load YouTube videos
  - [ж] Add custom description for all pages
  - [x] Add canonical URL
  - [x] Fix redirects. All link to end with /
  - [x] Add Open Graph tags for Facebook
- [x] Publish to ndebg.com
- [x] Make [Facebook page](https://www.facebook.com/ndebgcom/)
- [x] Add links to FB page
- [x] Republish website